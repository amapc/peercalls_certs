#!/usr/bin/env node

'use strict'
const config = require('config')
const turn = require('../turn.js')
const router = require('express').Router()
const uuid = require('uuid')

const cfgIceServers = config.get('iceServers')

const passport = require('passport')
const jwt = require('jsonwebtoken')


router.get('/', (req, res) => {
  let prefix = 'call/'
  if (req.originalUrl.charAt(req.originalUrl.length - 1) === '/') prefix = ''
  res.redirect(prefix + uuid.v4())
})

router.get('/:callId', (req, res) => {
  const token = getToken(req.headers);
  console.log(req.headers);
  if (req.headers.verified == 'SUCCESS') {
    const iceServers = turn.processServers(cfgIceServers)
    res.render('call', {
      callId: encodeURIComponent(req.params.callId),
      iceServers
    })
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized'
    })
  }
})
getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router
