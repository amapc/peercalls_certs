# Peer Calls
Fork from [https://github.com/jeremija/peer-calls](https://github.com/jeremija/peer-calls) with client certificates support.

## Info
Modified files: `src/server/routes/call.js`


http://blog.theodo.fr/2015/09/protect-your-node-js-api-with-nginx-and-ssl-client-certificates/

---

WebRTC peer to peer calls for everyone. See it live in action at
[peercalls.com](https://peercalls.com).

Work in progress.

# Requirements
 - Node.js 8 [https://nodejs.org/en/](https://nodejs.org/en/)

# Installation & Running

From git source:

```bash
git clone https://github.com/jeremija/peer-calls.git
cd peer-calls
npm install

# for production
npm start
npm run build

# for development
npm run start:watch
```

To run a development version, type:



If you successfully completed the above steps, your commandline/terminal should
show that your node server is listening.

On your other machine or mobile device open the url:

```bash
http://<your_ip_or_localhost>:3000

# Testing

```bash
npm install
npm test
```

# Browser Support

Tested on Firefox and Chrome, including mobile versions.

Does not work on iOS 10, but should work on iOS 11 (untested).

For more details, see here:

- http://caniuse.com/#feat=rtcpeerconnection
- http://caniuse.com/#search=getUserMedia

# Contributing

See [Contributing](CONTRIBUTING.md) section.

# License

[MIT](LICENSE)
